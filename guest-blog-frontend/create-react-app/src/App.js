import React from 'react';

import Home from './Home';
import CreatePost from './CreatePost';
import Post from './Post';
import {BrowserRouter as Router, Link as RouterLink, Route, Switch,} from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
}));

export default function App() {

    const classes = useStyles();

    return (
        <Router>
            <React.Fragment>
                <CssBaseline/>
                <AppBar position="relative">
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            <RouterLink to="/" style={{textDecoration: 'none', color: "#ffffff"}}>Guest
                                Blog</RouterLink>
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div>
                    <Switch>
                        <Route path="/post/:id" children={<Post classes={classes}/>}/>
                        <Route path="/create_post">
                            <CreatePost classes={classes}/>
                        </Route>
                        <Route path="/">
                            <Home classes={classes}/>
                        </Route>
                    </Switch>
                </div>
                {/* Footer */}
                <footer className={classes.footer}>
                    <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                        Test Assignment
                    </Typography>
                </footer>
                {/* End footer */}
            </React.Fragment>
        </Router>

    );
}
