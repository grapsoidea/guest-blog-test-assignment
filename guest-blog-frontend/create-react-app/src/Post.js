import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import {useParams} from "react-router-dom";


const POSTS_SERVICE_URL = "http://localhost:8080/api/posts/";
const COMMENTS_SERVICE_URL = "http://localhost:8080/api/comments/";
const options = {
    headers: {
        'Access-Control-Allow-Origin': '*'
    }
};

export default function Post(props) {

    const [isFetching, setIsFetching] = useState(true);
    const [post, setPost] = useState({
        id: "",
        title: "",
        date: "",
        picture: "",
        comments: []
    });
    const [error, setError] = useState(null);

    let {id} = useParams();

    const fetchPost = () => {
        setIsFetching(true);
        fetch(POSTS_SERVICE_URL + id, options)
            .then(response => {
                if (response.status !== 200) {
                    throw new Error(response.status);
                }
                return response;
            })
            .then(response => response.json())
            .then(result => {
                result.comments = result.comments.reverse();
                setPost(result);
                setIsFetching(false)
            })
            .catch(e => {
                console.error(e);
                setIsFetching(false);
                setError(e);

            });
    };

    useEffect(() => {
        fetchPost();
        //    setInterval(() => fetchPost(), 5000);
    }, []);

    const [userNameComment, setUserNameComment] = useState('');
    const handleChangeUserNameComment = event => setUserNameComment(event.target.value);

    const [contentComment, setContentComment] = useState('');
    const handleChangeContentComment = event => setContentComment(event.target.value);

    const validatoin = event => {
        if (userNameComment === '')
            alert("\"Your nick\" field must not be empty");
        else if (contentComment === '')
            alert("\"Message\" field must not be empty");
        else {
            handleSubmit(event);
            if (!error)
                window.location.reload()
        }
    };

    const handleSubmit = event => {
        const data = {
            "postId": id,
            "userName": userNameComment,
            "content": contentComment
        };
        fetch(COMMENTS_SERVICE_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
            'Access-Control-Allow-Origin': '*',

        })
            .then(response => response.json())
            .catch(e => console.log(e));

        event.preventDefault();
    };

    const classes = props.classes;

    if (isFetching) return (
        <Typography gutterBottom variant="h5" align="center" component="h2" style={{marginTop: 50}}>
            Loading...
        </Typography>
    );

    if (error) return (
        <Typography gutterBottom variant="h5" align="center" component="h2" style={{marginTop: 50}}>
            {`Error: ${error.message}`}
        </Typography>
    );

    return (
        <main>
            {/* Hero unit */}
            <div className={classes.heroContent}>
                <Container maxWidth="lg">
                    <Box style={{margin: 8}}>
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.cardMedia}
                                image={post.picture}
                                title="Image title"
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {post.title}
                                </Typography>
                                <Typography style={{marginTop: 20, whiteSpace: 'pre-line'}}>
                                    {post.content}
                                </Typography>
                            </CardContent>
                        </Card>
                    </Box>
                </Container>
                <Container maxWidth="md">
                    <TextField
                        required
                        id="nick"
                        label="Your nick"
                        variant="outlined"
                        style={{margin: 8, marginTop: 20}}
                        value={userNameComment}
                        onChange={handleChangeUserNameComment}
                    />
                    <Box
                        display="flex"
                        style={{margin: 8, marginTop: 0}}
                    >
                        <Box flexGrow={1}>
                            <TextField
                                required
                                id="filled-multiline-flexible"
                                label="Message"
                                multiline
                                rowsMax="4"
                                variant="filled"
                                fullWidth
                                value={contentComment}
                                onChange={handleChangeContentComment}
                            />
                        </Box>
                        <Box>
                            <Button
                                variant="contained"
                                color="primary"
                                style={{marginLeft: 8}}
                                onClick={validatoin}
                            >
                                Send
                            </Button>
                        </Box>
                    </Box>
                    <Box
                        style={{margin: 8, marginTop: 32}}
                    >
                        <Box>
                            {post.comments.map(comment => (
                                <TextField
                                    id="filled-multiline-flexible"
                                    key={comment.date}
                                    label={comment.userName}
                                    value={comment.content}
                                    multiline
                                    rowsMax="4"
                                    fullWidth
                                    InputProps={{
                                        readOnly: true,
                                    }}
                                    style={{marginTop: 8, marginBottom: 8}}
                                    variant="outlined"
                                />

                            ))}
                        </Box>
                        <Box>

                        </Box>
                    </Box>
                </Container>
            </div>
        </main>
    );
}