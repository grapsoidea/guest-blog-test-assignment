import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {Redirect} from "react-router-dom";


const POSTS_SERVICE_URL = "http://localhost:8080/api/posts";
const options = {
    headers: {
        'Access-Control-Allow-Origin': '*'
    }
};

export default function Home(props) {

    const [isFetching, setIsFetching] = useState(true);
    const [posts, setPosts] = useState([]);
    const [error, setError] = useState(null);
    const [redirectTo, setRedirectTo] = useState(null);
    const handleRedirect = (value) => {
        setRedirectTo(value)
    };

    const fetchPosts = () => {
        setIsFetching(true);
        fetch(POSTS_SERVICE_URL, options)
            .then(response => {
                if (response.status !== 200) {
                    throw new Error(response.status);
                }
                return response;
            })
            .then(response => response.json())
            .then(result => {
                result = result.map(post => {
                    let title = post.title;
                    let content = post.content;
                    if (title.length > 25) {
                        title = title.slice(0, 25).split(' ').slice(0, -1).join(' ') + "...";
                    }
                    if (content.length > 100) {
                        content = content.slice(0, 100).split(' ').slice(0, -1).join(' ') + "...";
                    }
                    return {
                        "id": post.id,
                        "title": title,
                        "content": content,
                        "picture": post.picture
                    }
                });
                setPosts(result);
                setIsFetching(false)
            })
            .catch(e => {
                console.error(e);
                setIsFetching(false);
                setError(e);

            });
    };

    useEffect(() => {
        fetchPosts();
        // setInterval(() => fetchPosts(), 5000);
    }, []);

    const classes = props.classes;

    if (isFetching) return (
        <main>
            <Typography gutterBottom variant="h5" align="center" component="h2" style={{marginTop: 50}}>
                Loading...
            </Typography>
        </main>
    );

    if (error) return (
        <main>
            <Typography gutterBottom variant="h5" align="center" component="h2" style={{marginTop: 50}}>
                {`Error: ${error.message}`}
            </Typography>
        </main>
    );


    if (redirectTo) {
        return <Redirect to={redirectTo}/>
    }

    return (
        <main>
            {/* Hero unit */}
            <div>
                <Container maxWidth="sm">
                    <div className={classes.heroButtons}>
                        <Grid container justify="center">
                            <Grid item>
                                <Button variant="contained" color="primary"
                                        onClick={() => handleRedirect("/create_post/")}>
                                    Create Post
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </div>
            <Container className={classes.cardGrid} maxWidth="md">
                {/* End hero unit */}
                <Grid container spacing={4}>
                    {posts.map(post => (
                        <Grid item key={post.id} xs={12} sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image={post.picture}
                                    title="Image title"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {post.title}
                                    </Typography>
                                    <Typography>
                                        {post.content}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Button size="small" color="primary"
                                            onClick={() => handleRedirect("/post/" + post.id)}>
                                        View
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </main>
    );
}