import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import {Redirect} from "react-router-dom";


const POSTS_SERVICE_URL = "http://localhost:8080/api/posts";


export default function CreatePost(props) {

    const [error, setError] = useState(null);
    const [redirect, setRedirect] = useState(false);
    const [postId, setPostId] = useState("");

    const [titlePost, setTitlePost] = useState('');
    const handleChangeTitlePost = event => setTitlePost(event.target.value);

    const [contentPost, setContentPost] = useState('');
    const handleChangeContentPost = event => setContentPost(event.target.value);

    const [picturePost, setPicturePost] = useState('');
    const handleChangePicturePost = event => setPicturePost(event.target.value);


    const validatoin = event => {
        if (titlePost === '')
            alert("\"Title of Post\" field must not be empty");
        else if (contentPost === '')
            alert("\"Content of Post\" field must not be empty");
        else if (picturePost === '')
            alert("\"Picture URL\" field must not be empty");
        else {
            handleSubmit(event);
        }
    };

    const handleSubmit = event => {
        const data = {
            "title": titlePost,
            "content": contentPost,
            "picture": picturePost
        };
        fetch(POSTS_SERVICE_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
            'Access-Control-Allow-Origin': '*',

        })
            .then(response => {
                if (response.status !== 200) {
                    throw new Error(response.status);
                }
                return response;
            })
            .then(response => response.json())
            .then(result => {
                setRedirect(true);
                setPostId(result.id);
            })
            .catch(e => {
                console.error(e);
                setError(e);

            });
        event.preventDefault();
        if (redirect) {
            return <Redirect to={'/post/' + postId}/>
        }
    };

    const classes = props.classes;

    if (error) return (
        <main>
            <Typography gutterBottom variant="h5" align="center" component="h2" style={{marginTop: 50}}>
                {`Error: ${error.message}`}
            </Typography>
        </main>
    );

    if (redirect & postId !== "") {
        return <Redirect to={'/post/' + postId}/>
    }

    return (
        <main>
            {/* Hero unit */}
            <div className={classes.heroContent}>
                <Container width="75%">
                    <Typography variant="h4" style={{margin: 8}}>
                        Create Post
                    </Typography>
                    <TextField
                        required
                        id="title-of-post"
                        label="Title of Post"
                        variant="filled"
                        fullWidth
                        style={{margin: 8}}
                        value={titlePost}
                        onChange={handleChangeTitlePost}
                    />
                    <TextField
                        required
                        id="content-of-post"
                        label="Content of Post"
                        multiline
                        rows="20"
                        variant="filled"
                        fullWidth
                        style={{margin: 8}}
                        value={contentPost}
                        onChange={handleChangeContentPost}
                    />
                    <Box
                        display="flex"
                        style={{margin: 8}}
                    >
                        <Box>
                            <TextField
                                required
                                id="picture-of-post"
                                label="Picture URL"
                                variant="filled"
                                value={picturePost}
                                onChange={handleChangePicturePost}
                            />
                        </Box>
                        <Box flexGrow={1}>
                        </Box>
                        <Box>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={validatoin}
                            >
                                Create
                            </Button>
                        </Box>
                    </Box>
                </Container>
            </div>
        </main>
    );
}