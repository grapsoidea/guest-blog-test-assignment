package com.example.guestblog.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PostNotFoundExeption extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PostNotFoundExeption(String id) {
        
        super("Could not find post" + id);
    }
    
}