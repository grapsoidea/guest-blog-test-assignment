package com.example.guestblog.exceptions;


public class CommentNotAddedExeption extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    public CommentNotAddedExeption() {
        
        super("Could not add comment");
    }
    
}