package com.example.guestblog.repositories;

import com.example.guestblog.models.*;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PostRepository extends MongoRepository<Post, String>, CommentRepositoryCustom {

}