package com.example.guestblog.repositories;

import java.util.Optional;

import com.example.guestblog.models.Comment;
import com.example.guestblog.models.Post;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class CommentRepositoryCustomImpl implements CommentRepositoryCustom {
 
    private final MongoOperations mongoOperations;

    CommentRepositoryCustomImpl(MongoOperations mongoOperations) {
        
        this.mongoOperations = mongoOperations;
    }

    public Optional<Comment> pushComment(String id, Comment comment) {
        
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update = new Update();
        update.push("comments", comment);
        if (mongoOperations.updateFirst(query, update, Post.class).getModifiedCount() != 0) {

            return Optional.of(comment);
        }
        else {
        
            return Optional.empty();
        }
    }
}