package com.example.guestblog.repositories;

import java.util.Optional;

import com.example.guestblog.models.Comment;

public interface CommentRepositoryCustom {

    public Optional<Comment> pushComment(String id, Comment comment);
}