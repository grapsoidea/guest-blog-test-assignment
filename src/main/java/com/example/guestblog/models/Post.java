package com.example.guestblog.models;

import java.time.Instant;
import java.util.List;

import org.springframework.data.annotation.Id;

public class Post {

    @Id
    private String id;

    private String title;
    private Instant date;
    private String content;
    private String picture;
    private List<Comment> comments;

    public String getId() {
        
        return id;
    }

    public Instant getDate() {
        
        return date;
    }

    public void setDate(Instant date) {
        
        this.date = date;
    }

    public String getContent() {
        
        return content;
    }

    public void setContent(String content) {
        
        this.content = content;
    }

    public String getPicture() {
        
        return picture;
    }

    public void setPicture(String picture) {
        
        this.picture = picture;
    }

    public List<Comment> getComments() {
        
        return comments;
    }

    public void setComments(List<Comment> comments) {
        
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        
        this.comments.add(comment);
    }

    public String getTitle() {
        
        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public Post(String title, Instant date, String content, String picture, List<Comment> comments) {

        this.title = title;
        this.date = date;
        this.content = content;
        this.picture = picture;
        this.comments = comments;
    }
    
}
