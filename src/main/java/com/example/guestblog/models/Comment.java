package com.example.guestblog.models;

import java.time.Instant;

public class Comment {
    
    private String userName;
    private Instant date;
    private String content;

    public String getUserName() {
        
        return userName;
    }

    public void setUserName(String userName) {
        
        this.userName = userName;
    }

    public Instant getDate() {
        
        return date;
    }

    public void setDate(Instant date) {
        
        this.date = date;
    }

    public String getContent() {
        
        return content;
    }

    public void setContent(String content) {
        
        this.content = content;
    }

    public Comment(String userName, Instant date, String content) {
        
        this.userName = userName;
        this.date = date;
        this.content = content;
    }

}