package com.example.guestblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuestBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuestBlogApplication.class, args);
	}

}
