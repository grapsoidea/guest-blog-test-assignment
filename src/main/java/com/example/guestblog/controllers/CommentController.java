package com.example.guestblog.controllers;

import java.time.Instant;

import javax.validation.Valid;

import com.example.guestblog.exceptions.*;
import com.example.guestblog.controllers.dto.CommentDTO;
import com.example.guestblog.models.Comment;
import com.example.guestblog.repositories.PostRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class CommentController {
    private final PostRepository repository;

    CommentController(PostRepository repository) {
        
        this.repository = repository;
    }

    @PostMapping("/comments")
    Comment addComment(@RequestBody @Valid CommentDTO commentDTO) {

        Comment comment = new Comment(commentDTO.getUserName(), Instant.now(), commentDTO.getContent()); 
        return repository.pushComment(commentDTO.getPostId(), comment).orElseThrow(() -> new CommentNotAddedExeption());
    }
}