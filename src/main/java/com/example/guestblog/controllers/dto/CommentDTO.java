package com.example.guestblog.controllers.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CommentDTO {
    @NotNull
    @NotEmpty
    private String postId;

    @NotNull
    @NotEmpty
    private String userName;

    @NotNull
    @NotEmpty
    private String content;

    public String getPostId() {
        
        return postId;
    }

    public void setPostId(String postId) {
        
        this.postId = postId;
    }

    public String getUserName() {
        
        return userName;
    }

    public void setUserName(String userName) {
        
        this.userName = userName;
    }

    public String getContent() {
        
        return content;
    }

    public void setContent(String content) {
        
        this.content = content;
    }
}