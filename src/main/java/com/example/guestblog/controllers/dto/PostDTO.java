package com.example.guestblog.controllers.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PostDTO {
    @NotNull
    @NotEmpty
    private String title;
    @NotNull
    @NotEmpty
    private String content;
    @NotNull
    @NotEmpty
    private String picture;

    

    public String getTitle() {
        
        return title;
    }

    public void setTitle(String title) {
        
        this.title = title;
    }

    public String getContent() {
        
        return content;
    }

    public void setContent(String content) {
        
        this.content = content;
    }
    
    public String getPicture() {
        
        return picture;
    }

    public void setPicture(String picture) {
        
        this.picture = picture;
    }
}