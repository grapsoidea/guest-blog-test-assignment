package com.example.guestblog.controllers;

import com.example.guestblog.repositories.*;
import com.example.guestblog.models.*;
import com.example.guestblog.controllers.dto.PostDTO;
import com.example.guestblog.exceptions.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class PostController {
    private final PostRepository repository;

    PostController(PostRepository repository) {
        
        this.repository = repository;
    }
    
    @GetMapping("/posts")
    List<Post> getAllPosts() {
        
        return repository.findAll();
    }

    @PostMapping("/posts")
    Post createPost(@RequestBody @Valid PostDTO postDTO) {
        
        Post newPost = new Post(postDTO.getTitle(), Instant.now(), postDTO.getContent(), postDTO.getPicture(), new ArrayList<Comment>());

        return repository.save(newPost);
    }

    @GetMapping("/posts/{id}") 
    Post getOnePost(@PathVariable String id) {
        
        return repository.findById(id).orElseThrow(() -> new PostNotFoundExeption(id));
    }
    
}