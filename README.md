Guest Blog
===
It's test assignment that represents geust blog with following abilities:
* viewing posts
* creating posts
* submitting comments

This applicaton consists two part:
* backend
* frontend

Backend represents RESTfull (or almost)) applicaton that is designed using Spring Boot framework and MongoDB database.
Frontend is designed using React.js framework and MATERIAL-UI React components.

Backend 
---
Dependencies:

* java 8
* mongodb

Build and run instruction:

	cd /backend_root_example/
	./mvnw clean package
	java -jar target/guest-blog-0.0.1-SNAPSHOT.jar 

Backend API:
---

### Getting all posts
GET URL: **/posts/**
Response: 

	[ {
		"id": "123",
		"title": "Title",
		"date": "2019-12-10T21:14:32.019Z",
		"content": "Content",
		"picture": "https://example.com/1.jpg",
		"comments":
		[ {
			"userName": "UserName",
			"date": "2019-12-10T21:16:34.141Z",
			"content": "Message"
		}, ]
	}, ]

### Getting post with ID
GET URL: **/posts/123**
Response:

	{
		"id": "123",
		"title": "Title",
		"date": "2019-12-10T21:14:32.019Z",
		"content": "Content",
		"picture": "https://example.com/1.jpg",
		"comments": 
			[ {
			"userName": "UserName",
			"date": "2019-12-10T21:16:34.141Z",
			"content": "Message"
		}, ]
	}

### Creating post
POST URL: **/posts/**
Body:

	{
		"title": "Title",
		"content": "Content",
		"picture": "https://example.com/2.jpg",
	}

Response:

	{
		"id": "456",
		"title": "Title",
		"date": "2019-12-10T21:14:32.019Z",
		"content": "Content",
		"picture": "https://example.com/2.jpg",
		"comments": []
	}

### Submitting comment
POST URL: **/commets/**
Body:

	{  
		"postID": "456"
		"useName": "UserName",
		"content": "Content"
	}
Response:

	{  
		"postID": "456"
		"useName": "UserName",
		"content": "Content"
	}
Frontend
---
Dependencies:

* node ~v12.13
* npm ~v6.12
* yarn ~v1.19 (for building)

Build and run instruction:

	cd /fronend_root_example/
	npm install
	npm start
	#for building and workin together with backend 
	yarn build
	mv build /backend_root_example/resources/staitc

Frontend working examples:
---
![alt text](https://i.imgur.com/Kgd3byj.png)
![alt text](https://i.imgur.com/VgXhwTV.png)
![alt text](https://i.imgur.com/ao6A36U.png)
